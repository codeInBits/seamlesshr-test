<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->realText(250),
        'name' => $faker->words(2, true), 
        'code' => $faker->unique()->numerify('CD###'), 
        'unit' => $faker->numberBetween(1, 10),
    ];
});
