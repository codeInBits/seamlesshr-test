<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'courseName' => $this->name,
            'courseCode' => $this->code,
            'courseUnit' => $this->unit,
            'courseText' => $this->text,
            'createdAt' => (string) $this->created_at,
            'enrolledAt' => UserEnrolledResource::collection($this->whenLoaded('users')),
        ];
    }
}
