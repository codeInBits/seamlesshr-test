<?php

namespace App\Http\Controllers\API\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseResource;
use App\Jobs\CreateCourseJob;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    /**
     * Create courses.
     *
     * @param  Request  $request
     * @return CourseResource
     */
    protected function save(Request $request)
    {
        try {
            $course = CreateCourseJob::dispatch();
            return $this->successResponse($course, "Course(s) created successfully", 201);
        } catch (Exception $e) {
            return $this->fatalErrorResponse($e);
        }
    }

    public function export() 
    {
        return Excel::download(new CoursesExport, 'courses.xlsx');
    }
}
