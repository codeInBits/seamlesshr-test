<?php

namespace App\Http\Controllers\API\User\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\User\LoginRequest;
use App\Http\Resources\UserResource;
use App\User;
use Exception;
use JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function login(LoginRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $token = null;
            $token = JWTAuth::attempt($credentials);

            if (! $token) {
                return $this->errorResponse([], "Invalid Login Credentials", 401);
            }

            $response = [
                'token' => $token,
                'token_type' => 'Bearer',
                'expires_in' => JWTAuth::factory()->getTTL() * 60,
                'user' => new UserResource(JWTAuth::user()),
            ];
            
            return $this->successResponse($response, "Login was successful", 200);
        } catch (Exception $e) {
            return $this->fatalErrorResponse($e);
        }
    }
}
