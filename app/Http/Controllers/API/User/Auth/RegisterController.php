<?php

namespace App\Http\Controllers\API\User\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\UserResource;
use Exception;
use App\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Create a new User instance after a valid registration.
     *
     * @param  RegisterRequest  $request
     * @return UserResource
     */
    protected function register(RegisterRequest $request)
    {
        try {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);
            $response = new UserResource($user);

            return $this->successResponse($response, "User created successfully", 201);
        } catch (Exception $e) {
            return $this->fatalErrorResponse($e);
        }
    }
}
