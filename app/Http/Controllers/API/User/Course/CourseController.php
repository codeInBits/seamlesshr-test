<?php

namespace App\Http\Controllers\API\User\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterCourseRequest;
use App\Http\Resources\CourseResource;
use App\Jobs\CreateCourseJob;
use App\Course;
use JWTAuth;
use Exception;
use Carbon\Carbon;

class CourseController extends Controller
{
    /**
     * Create courses.
     *
     * @param  RegisterCourseRequest  $request
     * @return CourseResource
     */
    protected function register(RegisterCourseRequest $request)
    {
        try {
            $courseId = $request['courseId'];
            $result = Course::findOrFail($courseId);
            if (! $result) {
                return $this->errorResponse([], "One or more of the course Id do not exist", 404);
            }
            //Get authenticated user
            $user = JWTAuth::user();
            $user->courses()->attach($courseId, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

            return $this->successResponse($courseId, "The courses were successfully registered for the user", 201);
        } catch (Exception $e) {
            return $this->fatalErrorResponse($e);
        }
    }

     /**
     * Create courses.
     *
     * @param  Request $request
     * @return CourseResource
     */
    protected function getAll(Request $request)
    {
        try {
            $courses = Course::with('users')->paginate(10);

            $response = CourseResource::collection($courses)->response()->getData(true);
            return $this->successResponse($response, "List of all courses", 201);
        } catch (Exception $e) {
            return $this->fatalErrorResponse($e);
        }
    }
}
