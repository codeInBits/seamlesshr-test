<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'name', 'code', 'unit',
    ];

    /**
     * The users that belong to the course.
     */
    public function users()
    {
        return $this->belongsToMany(
            "App\User",
            "course_user",
            "course_id",
            "user_id"
        )
        ->wherePivot('user_id', JWTAuth::user()->id)
        ->withPivot([
            'created_at',
            'updated_at'
        ]);
    }
}
