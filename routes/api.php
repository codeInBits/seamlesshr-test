<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Users Routes
Route::group(
    [
        'prefix' => 'users',
        'as' => 'users.',
        'namespace' => 'API\User'
    ],
    function () {
        Route::group(
            [
                'namespace' => 'Auth'
            ],
            function () {
                Route::post('/register', 'RegisterController@register');
                Route::post('/login', 'LoginController@login');
            }
        );
        Route::group(
            [
                'prefix' => 'courses',
                'as' => 'courses.',
                'namespace' => 'Course',
                'middleware' => 'jwt.auth',
            ],
            function () {
                Route::post('/', 'CourseController@register');
                Route::get('/', 'CourseController@getAll');
            }
        );
    }
);

//Courses Routes
Route::group(
    [
        'prefix' => 'courses',
        'as' => 'courses.',
        'namespace' => 'API\Course'
    ],
    function () {
        Route::post('/', 'CourseController@save');
        Route::get('/export', 'CourseController@export');
    }
);